# About

Bash script to backup Hetzner Cloud Volumes. Automatically creates volumes for backups and maintains a defined number of them.

Steps performed:
1.  Query details about source volume
2.  Create and mount new volume (target volume, size is source volume size + 1)
3.  Copy source volume content to target volume (rsync)
4.  Unmount target volume
5.  Delete old backup volumes, if there are more than the maximum number to keep (see configuration)

# Installation

## Prerequisites

The following packages need to be installed for the script to run.

* Git
* [jq](https://stedolan.github.io/jq/)
* rsync

## Installation procedure

1.  Clone this repository
2.  Copy `example.config`, e. g. to `job1.config`, and adjust the settings
3.  Make sure `job1.config` is only readable to users performing the backup

# Usage

`backup.sh <config-file>`

## Example

`cd ~/hc-volume-backup`

`./backup.sh job1.config`
